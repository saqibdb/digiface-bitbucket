//
//  JPSemiModalAnimatedTransition.h
//  DigiFaces
//
//  Created by James on 3/20/16.
//  Copyright © 2016 INET360. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JPSemiModalAnimatedTransition : NSObject <UIViewControllerAnimatedTransitioning>
@property (nonatomic, assign) BOOL presenting;
@end