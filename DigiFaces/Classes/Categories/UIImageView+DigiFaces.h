//
//  UIImageView+DigiFaces.h
//  DigiFaces
//
//  Created by James on 3/20/16.
//  Copyright © 2016 INET360. All rights reserved.
//

#import <UIKit/UIKit.h>

@class UserInfo;

@interface UIImageView (DigiFaces)

- (void)setAvatarURL:(NSURL*)url;
- (void)setAvatarURLString:(NSString*)urlString;

- (void)setAvatarFromUserInfo:(UserInfo*)userInfo;

@end
