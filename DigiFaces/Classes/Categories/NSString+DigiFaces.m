//
//  NSString+DigiFaces.m
//  DigiFaces
//
//  Created by James on 3/15/16.
//  Copyright © 2016 INET360. All rights reserved.
//

#import "NSString+DigiFaces.h"
#import "NSString+FontAwesome.h"

@implementation NSString (DigiFaces)

+ (NSString*)fontAwesomeIconStringForIconKey:(NSString*)iconKey {
    NSString *iconIdentifier = DFLocalizedString(iconKey, nil);
    NSString *iconString = [NSString fontAwesomeIconStringForIconIdentifier:iconIdentifier];
    return iconString;
}

+ (NSString*)stringPrefixedWithIcon:(NSString*)iconIdentifier withLocalizedKey:(NSString*)localizedKey {
    NSString *iconString = [NSString fontAwesomeIconStringForIconIdentifier:iconIdentifier];
    NSString *titleString = DFLocalizedString(localizedKey, nil);
    return [NSString stringWithFormat:@"%@ %@", iconString, titleString];
}

+ (NSString*)stringPrefixedWithIconForKey:(NSString*)iconKey withLocalizedKey:(NSString*)localizedKey {
    NSString *fontAwesomeIdentifier = DFLocalizedString(iconKey, nil);
    return [NSString stringPrefixedWithIcon:fontAwesomeIdentifier withLocalizedKey:localizedKey];
}

- (NSString*)localizedAndPrefixedWithFontAwesomeIcon:(NSString*)iconIdentifier {
    return [NSString stringPrefixedWithIcon:iconIdentifier withLocalizedKey:self];
}

- (NSString*)localizedAndPrefixedWithIconForKey:(NSString*)iconKey {
    return [NSString stringPrefixedWithIconForKey:iconKey withLocalizedKey:self];
}

- (NSString*)stringByPrependingIconForKey:(NSString *)iconKey {
    NSString *iconString = [NSString fontAwesomeIconStringForIconKey:iconKey];
    return [iconString stringByAppendingString:self];
}

@end
