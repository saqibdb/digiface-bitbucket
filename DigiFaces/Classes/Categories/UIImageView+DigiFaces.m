//
//  UIImageView+DigiFaces.m
//  DigiFaces
//
//  Created by James on 3/20/16.
//  Copyright © 2016 INET360. All rights reserved.
//

#import "UIImageView+DigiFaces.h"

#import "UserInfo.h"
#import "File.h"

#import <SDWebImage/UIImageView+WebCache.h>

@implementation UIImageView (DigiFaces)

- (void)setAvatarURL:(NSURL *)url {
    if (url) {
        [self sd_setImageWithURL:url];
    } else {
        [self sd_setImageWithURL:[NSURL URLWithString:DFAvatarGenericImageURLKey] placeholderImage:[UIImage imageNamed:@"genericavatar"]];
    }
}

- (void)setAvatarURLString:(NSString*)urlString {
    [self setAvatarURL:urlString ? [NSURL URLWithString:urlString] : nil];
}

- (void)setAvatarFromUserInfo:(UserInfo *)userInfo {
    [self setAvatarURLString:userInfo.avatarFile.filePath];
}

@end
