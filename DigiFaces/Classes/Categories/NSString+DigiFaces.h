//
//  NSString+DigiFaces.h
//  DigiFaces
//
//  Created by James on 3/15/16.
//  Copyright © 2016 INET360. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (DigiFaces)

+ (NSString*)fontAwesomeIconStringForIconKey:(NSString*)localKey;
+ (NSString*)stringPrefixedWithIcon:(NSString*)iconIdentifier withLocalizedKey:(NSString*)localizedKey;
+ (NSString*)stringPrefixedWithIconForKey:(NSString*)iconKey withLocalizedKey:(NSString*)localizedKey;
- (NSString*)localizedAndPrefixedWithFontAwesomeIcon:(NSString*)iconIdentifier;
- (NSString*)localizedAndPrefixedWithIconForKey:(NSString*)iconKey;
- (NSString*)stringByPrependingIconForKey:(NSString*)iconKey;

@end
