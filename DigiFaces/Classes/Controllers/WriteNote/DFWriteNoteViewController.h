//
//  DFWriteNoteViewController.h
//  DigiFaces
//
//  Created by James on 3/19/16.
//  Copyright © 2016 INET360. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddCommentAction.h"

#import "DFWriteNoteViewControllerDelegate.h"

typedef enum : NSUInteger {
    kDFWriteNoteViewStateFullScreen,
    kDFWriteNoteViewStateMinimized
} kDFWriteNoteViewState;

@interface DFWriteNoteViewController : UIViewController

@property (nonatomic, weak) id<DFWriteNoteViewControllerDelegate> delegate;

@property (nonatomic, strong) AddCommentAction *action;

@property (nonatomic) kDFWriteNoteViewState viewState;

@property (nonatomic, weak) IBOutlet UITextView *commentBodyTextView;

@property (nonatomic, weak) IBOutlet UIButton *viewResponseButton;

- (instancetype)initWithAction:(AddCommentAction*)action;

@end
