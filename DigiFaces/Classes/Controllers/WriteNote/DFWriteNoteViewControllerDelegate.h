//
//  DFWriteNoteViewControllerDelegate.h
//  DigiFaces
//
//  Created by James on 3/19/16.
//  Copyright © 2016 INET360. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DFCommentType.h"

@class DFWriteNoteViewController;
@protocol DFWriteNoteViewControllerDelegate <NSObject>

- (void)writeNoteViewController:(DFWriteNoteViewController*)writeNoteViewController didGetServerResponseWithComment:(id<DFCommentType>)comment error:(NSError*)error;
- (void)writeNoteViewControllerDidRequestDismissal:(DFWriteNoteViewController *)writeNoteViewController;
- (void)writeNoteViewControllerDidRequestResponsePreview:(DFWriteNoteViewController *)writeNoteViewController;
- (void)writeNoteViewControllerDidRequestFullScreen:(DFWriteNoteViewController*)writeNoteViewController;
- (NSNumber*)threadId;

@end
