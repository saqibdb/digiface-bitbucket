//
//  DFWriteNoteViewController.m
//  DigiFaces
//
//  Created by James on 3/19/16.
//  Copyright © 2016 INET360. All rights reserved.
//

#import "DFWriteNoteViewController.h"

#import "UITextView+Placeholder.h"

#import "CustomAlertView.h"

#import "MBProgressHUD.h"


@interface DFWriteNoteViewController () {
    __block BOOL _isFullyVisible;
    __block BOOL _isAnimating;
}
@property (weak, nonatomic) IBOutlet UINavigationItem *myNavigationItem;
@property (strong, nonatomic) CustomAlertView *customAlertView;
@property (strong, nonatomic) UITapGestureRecognizer *tapGestureRecognizer;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewResponseContainerViewBottomSpaceConstraint;
@property (weak, nonatomic) IBOutlet UIView *viewResponseContainerView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@end

@implementation DFWriteNoteViewController

- (instancetype)initWithAction:(AddCommentAction *)action {
    self = [super initWithNibName:@"DFWriteNoteViewController" bundle:nil];
    if (self) {
        self.action = action;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _viewState = kDFWriteNoteViewStateFullScreen;
    
    self.navigationItem.leftBarButtonItem = self.myNavigationItem.leftBarButtonItem;
    self.navigationItem.rightBarButtonItem = self.myNavigationItem.rightBarButtonItem;
    
    if (self.navigationController) {
        self.navigationController.navigationBarHidden = true;
    }
    [self.view addGestureRecognizer:self.tapGestureRecognizer];
    self.tapGestureRecognizer.enabled = false;
    
    self.viewState = kDFWriteNoteViewStateFullScreen;
    
    [self.commentBodyTextView becomeFirstResponder];
    
    [self localizeUI];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    CGRect navBarFrame = self.navigationController.navigationBar.frame;
    navBarFrame.origin.y += 20.0;
    navBarFrame.size.height -= 20.0;
    
    self.navigationController.navigationBar.frame = navBarFrame;
}

- (void)localizeUI {
    [self.viewResponseButton setTitle:DFLocalizedString(@"view.add_note.button.view_response", nil) forState:UIControlStateNormal];
    self.commentBodyTextView.placeholder = DFLocalizedString([self.action actionNameKey], nil);
    self.titleLabel.text = [DFLocalizedString([self.action objectNameKey], nil) uppercaseString];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Animation / View State
- (void)setViewState:(kDFWriteNoteViewState)viewState {
    CGFloat viewResponseConstraint;
    switch (viewState) {
        case kDFWriteNoteViewStateFullScreen: {
            // normal state.  all visible.
            viewResponseConstraint = 0;
            // no need for tap GR because its function is to request full screen
            self.tapGestureRecognizer.enabled = false;
            
        } break;
        case kDFWriteNoteViewStateMinimized: {
            // minimized state.  no "view response" button
            viewResponseConstraint = -self.viewResponseContainerView.frame.size.height;
            self.tapGestureRecognizer.enabled = true;
            
        } break;
        default:
            break;
    }
    
    
    [UIView animateWithDuration:kDFAnimationDuration animations:^{
        self.viewResponseContainerViewBottomSpaceConstraint.constant = viewResponseConstraint;
        [self.view layoutIfNeeded];
    }];
}

#pragma mark - UITextViewDelegate
- (void)textViewDidBeginEditing:(UITextView *)textView {
    [self takeFullScreen];
}

#pragma mark - IBActions

- (IBAction)dismiss {
    self.commentBodyTextView.text = nil;
    if ([self.delegate respondsToSelector:@selector(writeNoteViewControllerDidRequestDismissal:)]) {
        [self.delegate writeNoteViewControllerDidRequestDismissal:self];
    }
}

- (IBAction)requestPreview:(id)sender {
    if ([self.delegate respondsToSelector:@selector(writeNoteViewControllerDidRequestResponsePreview:)]) {
        [self.delegate writeNoteViewControllerDidRequestResponsePreview:self];
        [self.commentBodyTextView resignFirstResponder];
    }
}

- (IBAction)sendNote:(id)sender {
    if (self.commentBodyTextView.text.length > 0) {
        
        defwself
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [self.action executeWithCommentText:self.commentBodyTextView.text threadId:[self.delegate threadId] completion:^(id<DFCommentType> comment, NSError *error) {
            defsself
            if (sself) {
                [MBProgressHUD hideHUDForView:sself.view animated:YES];
                if (error) {
                    [sself.customAlertView showAlertWithMessage:DFLocalizedString(@"view.response.error.comment_note_send_failure", nil) inView:sself.view withTag:0];
                } else {
                    [sself dismiss];
                    if ([sself.delegate respondsToSelector:@selector(writeNoteViewController:didGetServerResponseWithComment:error:)]) {
                        [sself.delegate writeNoteViewController:self didGetServerResponseWithComment:comment error:error];
                    }
                }
            }
        }];
    }
}

#pragma mark - UIGestureRecognizers
- (UITapGestureRecognizer*)tapGestureRecognizer {
    if (!_tapGestureRecognizer) {
        _tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(takeFullScreen)];
    }
    return _tapGestureRecognizer;
}


#pragma mark - Misc View Controls

- (void)takeFullScreen {
    if ([self.delegate respondsToSelector:@selector(writeNoteViewControllerDidRequestFullScreen:)]) {
        [self.delegate writeNoteViewControllerDidRequestFullScreen:self];
    }
}


- (CustomAlertView*)customAlertView {
    if (!_customAlertView) {
        _customAlertView = [[CustomAlertView alloc]initWithNibName:@"CustomAlertView" bundle:nil];
        [_customAlertView setSingleButton:YES];
    }
    return _customAlertView;
}


- (void)slideToTargetY:(CGFloat)y completion:(void (^)(BOOL finished))completion {
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y = y;
    viewFrame.size.height = [UIScreen mainScreen].bounds.size.height-y;
    [UIView animateWithDuration:kDFAnimationDuration animations:^{
        self.view.frame = viewFrame;
    } completion:completion];
}

@end
