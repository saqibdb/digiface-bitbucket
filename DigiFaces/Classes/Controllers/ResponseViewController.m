//
//  ResponseViewController.m
//  DigiFaces
//
//  Created by confiz on 27/06/2015.
//  Copyright (c) 2015 Usasha studio. All rights reserved.
//

#import "ResponseViewController.h"

#import "MBProgressHUD.h"
#import "AFNetworking.h"
#import "Utility.h"

#import "Response.h"
#import "UserCell.h"
#import "ImagesCell.h"
#import "NotificationCell.h"
#import "DFCommentCell.h"
#import "DFResponseBodyCell.h"
#import "DFCommentSectionHeaderView.h"
#import "DFMediaCollectionViewCell.h"


#import "Comment.h"
#import "InternalComment.h"
#import "ResearcherComment.h"
#import "UIImageView+AFNetworking.h"
#import "TextAreaResponse.h"
#import "Utility.h"
#import "CustomAlertView.h"
#import "RTCell.h"
#import "DailyDiary.h"
#import "CarouselViewController.h"
#import "NSString+StripHTML.h"
#import "UILabel+setHTML.h"
#import "DiaryTheme.h"
#import "ImageGalleryResponse.h"
#import "Integer.h"
#import "Project.h"
#import "File.h"
#import "DFObjectSection.h"

#import "AddCommentController.h"
#import "DFWriteNoteViewController.h"
#import "DFWriteNoteViewControllerDelegate.h"

#import <SDWebImage/UIImageView+WebCache.h>
#import <WYPopoverController/WYPopoverController.h>
#import <HPGrowingTextView/HPGrowingTextView.h>
#import <MHVideoPhotoGallery/MHGallery.h>

#import "NSString+DigiFaces.h"
#import "UIImageView+DigiFaces.h"

typedef enum : NSUInteger {
    kDFResponseViewStatePreview,
    kDFResponseViewStateNormal
} kDFResponseViewState;

static int const kDFResponseBodyNumberOfMediaIconsPerRow = 5;
static CGFloat const kDFResponseBodyPaddingBetweenMediaCells = 8.0;
@interface ResponseViewController () <AddCommentControllerDelegate, ImageCellDelegate, HPGrowingTextViewDelegate, UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UIGestureRecognizerDelegate>
{
    NSInteger contentHeight;
    NSInteger selectedIndex;
    
    CGFloat _responseBodyCellHeight;
    CGFloat _mediaCollectionViewHeight;
    CGSize _responseBodyMediaIconSize;
    
    DFResponseBodyCell *_responseBodyCell;
    
    __block BOOL _isAnimating;
}

@property (nonatomic, retain) CustomAlertView * customAlert;
@property (nonatomic, strong) DailyDiary *dailyDiary;
@property (nonatomic, strong) NSNumber *threadId;
@property (nonatomic, strong) NSNumber *commentId;

@property (nonatomic, strong) NSString *alertMessageToShow;
@property (nonatomic, strong) NSIndexPath *indexPathToScrollTo;

@property (nonatomic, strong) NSArray<DFObjectSection<id<DFCommentType>>*> *commentSections;
@property (nonatomic, retain) NSArray<NSArray<NSNumber*>*> *cellHeights;
@property (nonatomic, strong) NSArray *comments;
@property (nonatomic, strong) NSArray *internalComments;
@property (nonatomic, strong) NSArray *researcherComments;
@property (nonatomic, strong) NSArray<File*> *responseBodyMediaFiles;
@property (nonatomic, strong) NSArray<MHGalleryItem*> *galleryItems;

@property (weak, nonatomic) IBOutlet UIButton *sendButton;
@property (weak, nonatomic) IBOutlet HPGrowingTextView *addCommentTextView;
@property (weak, nonatomic) IBOutlet UIButton *addNoteButton;

@property (nonatomic, strong) WYPopoverController *addCommentPopoverController;
@property (nonatomic, strong) AddCommentController<DFWriteNoteViewControllerDelegate> *addCommentController;

@property (nonatomic, strong) UITapGestureRecognizer *navBarTapGestureRecognizer;
@property (nonatomic, strong) UIPanGestureRecognizer *navBarPanGestureRecognizer;
@property (nonatomic, strong) UITapGestureRecognizer *tableViewTapGestureRecognizer;
@property (nonatomic, strong) UIPanGestureRecognizer *tableViewPanGestureRecognizer;

@property (nonatomic, strong) UITapGestureRecognizer *mediaCollectionViewTapGestureRecognizer;

@property (nonatomic, strong) DFWriteNoteViewController *writeNoteViewController;
@property (nonatomic, strong) MHGalleryController *galleryController;

@property (nonatomic) kDFResponseViewState viewState;

@end

@implementation ResponseViewController

#pragma mark - View Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self registerNibsForTableView:self.tableView];
    
    if (self.notification) {
        [self setupWithNotification:self.notification];
    } else {
        [self prepareAndLoadData];
    }
    
    self.viewState = kDFResponseViewStateNormal;
    
    // remove excess lines from table view if empty or too little content to see it all at once
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    // addCommentTextView styling
    self.addCommentTextView.layer.cornerRadius = 4.0f;
    self.addCommentTextView.clipsToBounds = true;
    self.addCommentTextView.delegate = self;
    
    // init addCommentController (to be removed)
    self.addCommentController = [[AddCommentController alloc] initWithStyle:UITableViewStylePlain];
    self.addCommentController.delegate = self;
    self.addCommentPopoverController = [[WYPopoverController alloc] initWithContentViewController:self.addCommentController];
    
    
    // permissions
    [self displayElementsBasedOnPermissions];
    
    
    [self localizeUI];
}

- (void)localizeUI {
    self.navigationItem.title = DFLocalizedString(@"view.response.navbar.title", nil);
    self.addCommentTextView.placeholder = DFLocalizedString(@"view.response.input.response", nil);
    [self.addNoteButton setTitle:[NSString fontAwesomeIconStringForIconKey:@"view.response.button.add_note.fa_key"] forState:UIControlStateNormal];
    //[self.sendButton setTitle:DFLocalizedString(@"view.response.button.send", nil) forState:UIControlStateNormal];
}

- (void)displayElementsBasedOnPermissions {
    if (![self.addCommentController shouldShowCommentInputTextView]) {
        self.commentTextViewHeightConstraint.constant = 0.0;
        self.messageTextView.hidden = true;
    } else {
        self.commentTextViewHeightConstraint.constant = 44.0;
        self.messageTextView.hidden = false;
    }
    [self.view layoutIfNeeded];
    self.addNoteButton.hidden = ![self.addCommentController shouldShowAddNoteButton];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self addGestureRecognizers];
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    if (self.addCommentPopoverController.isPopoverVisible) {
        [self.addCommentPopoverController dismissPopoverAnimated:true];
    }
    
    BOOL allRead = true;
    for (Comment *comment in self.diary.comments) {
        if (!comment.isRead.boolValue) {
            allRead = false;
            break;
        }
    }
    if (allRead) self.diary.isRead = @YES;
    
    //TODO: Figure out why we nil'd these out...
    // self.diary = nil;
    // self.dailyDiary = nil;
    
    // if ([self.delegate respondsToSelector:@selector(didSetDailyDiary:)]) {
    //     [self.delegate didSetDailyDiary:self.dailyDiary];
    // }
    
    [self removeGestureRecognizers];
    if (self.writeNoteViewController) {
        [self addCommentController:self.addCommentController didRequestWriteNoteViewControllerDismissal:self.writeNoteViewController];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Animation / View State

- (void)setViewState:(kDFResponseViewState)viewState {
    CGFloat targetYSplit = 0.0;
    switch (viewState) {
        case kDFResponseViewStateNormal: {
            // normal view state.  all things are visible and view is full-size
        } break;
            
        case kDFResponseViewStatePreview: {
            // preview view state.  hide comment text view and view is about half-size
        } break;
            
        default:
            break;
    }
}

- (void)setYSplit:(CGFloat)targetYSplit withViewController:(UIViewController*)viewController completion:(void (^)(BOOL success))completion {
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    CGRect otherViewFrame;
    
    otherViewFrame = viewController.view.frame;
    otherViewFrame.origin.y = targetYSplit;
    otherViewFrame.size.height = screenHeight - targetYSplit;
    
    [UIView animateWithDuration:kDFAnimationDuration animations:^{
        self.keyboardAvoidingView.keyboardHeight.constant = otherViewFrame.size.height - 20.0;
        [self.view layoutIfNeeded];
        viewController.view.frame = otherViewFrame;
        [viewController.view layoutIfNeeded];
    } completion:completion];
}

#pragma mark - Launch from Notification

- (BOOL)setupWithNotification:(Notification*)notification {
    BOOL error = false;
    if (notification) {
        if (self.notification.isDailyDiaryNotification.boolValue) {
            self.dailyDiary =  LS.myUserInfo.currentProject.dailyDiary;
            if (!self.dailyDiary) {
                // attempt to fetch diary
                Integer *diaryId = LS.myUserInfo.currentProject.dailyDiaryList.anyObject;
                if (!diaryId) {
                    error = true;
                    NSLog(@"ResponseViewController setupWithNotification: error - User info not loaded.");
                } else {
                    [self fetchDailyDiaryWithDiaryID:diaryId.integerValue];
                }
            } else {
                [self showNotificationContentFromDiary];
            }
        } else { // otherwise it's a diary theme.  load the thread.
            [self getResponsesWithActivityId:notification.activityId.integerValue];
        }
        if (error) {
            [self.customAlert showAlertWithMessage:DFLocalizedString(@"view.response.alert.content_not_found", nil) inView:self.navigationController.view withTag:0];
        }
    } else {
        error = true;
    }
    return error;
}



- (void)showNotificationContentFromDiary {
    NSNumber *threadId = self.notification.referenceId;
    NSNumber *commentId = self.notification.referenceId2;
    BOOL error;
    
    self.diary = [self.dailyDiary getResponseWithThreadID:threadId];
    if (!self.diary) {
        error = true;
        NSLog(@"ResponseViewController showNotificationContentFromDiary: error - Couldn't find a diary with thread id %@", threadId);
    } else {
        [self prepareAndLoadData];
        // now scroll to that comment
        for (NSInteger i = 0, n = self.diary.comments.count; i<n; ++i) {
            Comment *comment = self.comments[i];
            if ([comment.commentId isEqualToNumber:commentId]) {
                [self scrollToComment:comment];
                return;
            }
        }
        // if we made it this far, we couldn't find a comment with that id.
        error = true;
        NSLog(@"ResponseViewController showNotificationContentFromDiary: error - Couldn't find a comment with id %@", commentId);
    }
    if (error) {
        [self.customAlert showAlertWithMessage:DFLocalizedString(@"view.response.alert.content_not_found", nil) inView:self.navigationController.view withTag:0];
    }
}

#pragma mark - UI Misc
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
- (CustomAlertView*)customAlert {
    if (!_customAlert) {
        _customAlert = [[CustomAlertView alloc]initWithNibName:@"CustomAlertView" bundle:nil];
        [_customAlert setSingleButton:YES];
    }
    return _customAlert;
}

- (IBAction)cancelThis:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)registerNibsForTableView:(UITableView*)tableView {
    [tableView registerNib:[UINib nibWithNibName:@"DFResponseBodyCell" bundle:nil] forCellReuseIdentifier:[DFResponseBodyCell reuseIdentifier]];
    [tableView registerNib:[UINib nibWithNibName:@"DFCommentCell" bundle:nil] forCellReuseIdentifier:[DFCommentCell reuseIdentifier]];
    [tableView registerNib:[UINib nibWithNibName:@"DFCommentSectionHeaderView" bundle:nil] forHeaderFooterViewReuseIdentifier:[DFCommentSectionHeaderView reuseIdentifier]];
    
}



- (IBAction)sendComment:(id)sender {
    
    if ([_addCommentTextView.text isEqualToString:@""]) {
        return;
    }
    NSNumber *threadId;
    if (_diary) {
        threadId = _diary.threadId;
    }
    else if (_response){
        threadId = _response.threadId;
    }
    else return;
    
    [self addComment:_addCommentTextView.text withThreadId:threadId];
}

- (IBAction)addNote:(id)sender {
    
    if (self.writeNoteViewController == nil) {
        
        NSNumber *threadId;
        if (_diary) {
            threadId = _diary.threadId;
        }
        else if (_response){
            threadId = _response.threadId;
        }
        else return;
        
        [self.addCommentController addNoteWithThreadId:threadId];
    } else {
        [self addCommentController:self.addCommentController writeNoteControllerDidRequestFullScreen:self.writeNoteViewController];
    }
}

- (void)scrollToComment:(id<DFCommentType>)comment {
    // first we gotta find the comment...
    NSUInteger sectionIndex = 0;
    NSUInteger rowIndex;
    NSIndexPath *indexPath = nil;
    for (DFObjectSection<id<DFCommentType>>* commentSection in self.commentSections) {
        rowIndex = [commentSection.objects indexOfObject:comment];
        if (rowIndex != NSNotFound) {
            indexPath = [NSIndexPath indexPathForRow:rowIndex inSection:sectionIndex+1];
            break;
        }
        ++sectionIndex;
    }
    if (indexPath) {
        [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
        DFCommentCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
        // no completion block, cell's background color gets overwritten :(
        // [cell flash];
    }
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"carosulSegue"]){
        CarouselViewController * carouselController = [segue destinationViewController];
        carouselController.selectedIndex = selectedIndex;
        carouselController.files = [_diary.files allObjects];
    }
}

#pragma mark - MHPhotoVideoGallery

- (MHGalleryController*)galleryController {
    if (!_galleryController) {
        _galleryController = [[MHGalleryController alloc] initWithPresentationStyle:MHGalleryViewModeImageViewerNavigationBarHidden];
        __weak MHGalleryController *weakGallery = _galleryController;
        defwself
        _galleryController.finishedCallback = ^(NSInteger currentIndex,UIImage *image,MHTransitionDismissMHGallery *interactiveTransition, MHGalleryViewMode viewMode){
            __strong MHGalleryController *strongGallery = weakGallery;
            if (strongGallery) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [strongGallery dismissViewControllerAnimated:YES dismissImageView:nil completion:nil];
                    [interactiveTransition.moviePlayer stop];
                    defsself
                    if (sself) {
                        [[UIApplication sharedApplication] setStatusBarStyle:[sself preferredStatusBarStyle]];
                    }
                });
            }
        };
    }
    if (!_galleryController.galleryItems) {
        _galleryController.galleryItems = self.galleryItems;
    }
    return _galleryController;
}

- (void)setGalleryFilesWithMediaFiles:(NSArray<File*>*)mediaFiles {
    NSMutableArray *galleryItems = [NSMutableArray array];
    
    for (File *file in mediaFiles) {
        NSString *itemURLString = [file.filePathURL absoluteString];
        MHGalleryItem *galleryItem = [[MHGalleryItem alloc] initWithURL:itemURLString thumbnailURL:file.previewImageURL.path];
        galleryItem.galleryType = file.isVideo ? MHGalleryTypeVideo : MHGalleryTypeImage;
        [galleryItems addObject:galleryItem];
    }
    self.galleryItems = [NSArray arrayWithArray:galleryItems];
}

#pragma mark - Gesture Recognizers


//TODO: This should really be abstracted into a subclass, or into the comment text view container view
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return true;
}

- (void)handleGestureRecognizerNotOnCommentTextView:(UIGestureRecognizer*)gestureRecognizer {
    if (self.addCommentTextView.isFirstResponder) {
        [self.addCommentTextView resignFirstResponder];
    }
}

- (void)setupGestureRecognizer:(UIGestureRecognizer*)gr {
    gr.delegate = self;
}

- (UIPanGestureRecognizer*)panGestureRecognizer {
    UIPanGestureRecognizer *panGR = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handleGestureRecognizerNotOnCommentTextView:)];
    [self setupGestureRecognizer:panGR];
    return panGR;
}

- (UITapGestureRecognizer*)tapGestureRecognizer {
    UITapGestureRecognizer *tapGR = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleGestureRecognizerNotOnCommentTextView:)];
    tapGR.numberOfTapsRequired = 1;
    [self setupGestureRecognizer:tapGR];
    return tapGR;
}

- (UITapGestureRecognizer*)navBarTapGestureRecognizer {
    if (!_navBarTapGestureRecognizer) {
        _navBarTapGestureRecognizer = [self tapGestureRecognizer];;
    }
    return _navBarTapGestureRecognizer;
}
- (UIPanGestureRecognizer*)navBarPanGestureRecognizer {
    if (!_navBarPanGestureRecognizer) {
        _navBarPanGestureRecognizer = [self panGestureRecognizer];;
    }
    return _navBarPanGestureRecognizer;
}

- (UITapGestureRecognizer*)tableViewTapGestureRecognizer {
    if (!_tableViewTapGestureRecognizer) {
        _tableViewTapGestureRecognizer = [self tapGestureRecognizer];
    }
    return _tableViewTapGestureRecognizer;
}

- (UIPanGestureRecognizer*)tableViewPanGestureRecognizer {
    if (!_tableViewPanGestureRecognizer) {
        _tableViewPanGestureRecognizer = [self panGestureRecognizer];
    }
    return _tableViewPanGestureRecognizer;
}

- (void)addGestureRecognizers {
    [self.navigationController.navigationBar addGestureRecognizer:self.navBarTapGestureRecognizer];
    [self.navigationController.navigationBar addGestureRecognizer:self.navBarPanGestureRecognizer];
    [self.tableView addGestureRecognizer:self.tableViewTapGestureRecognizer];
    [self.tableView addGestureRecognizer:self.tableViewPanGestureRecognizer];
    
}

- (void)removeGestureRecognizers {
    [self.navigationController.navigationBar removeGestureRecognizer:self.navBarTapGestureRecognizer];
    [self.navigationController.navigationBar removeGestureRecognizer:self.navBarPanGestureRecognizer];
    [self.tableView removeGestureRecognizer:self.tableViewTapGestureRecognizer];
    [self.tableView removeGestureRecognizer:self.tableViewPanGestureRecognizer];
}


- (UITapGestureRecognizer*)mediaCollectionViewTapGestureRecognizer {
    if (!_mediaCollectionViewTapGestureRecognizer) {
        _mediaCollectionViewTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapMediaCollectionView:)];
    }
    return _mediaCollectionViewTapGestureRecognizer;
}

#pragma mark - Data Model Preparation

- (void)prepareAndLoadData {
    id contentObject = self.response ?: self.diary;
    
    [self organizeMediaFilesFromContentObject:contentObject];
    
    [self calculateResponseBodyDimensions];
    
    [self organizeCommentSectionsFromContentObject:contentObject];
    
    
    if (self.diary) {
        self.navigationItem.title = self.diary.title;
    }
    
    contentHeight = self.contentView.frame.size.height;
    
    [self.tableView reloadData];
    
}

// returns a pretty name for the comment section
- (NSString *)commentSectionTitleForKeyFragment:(NSString*)localKeyFragment withCommentArray:(NSArray<id<DFCommentType>>*)commentArray {
    NSString *localKeySuffix;
    NSUInteger numberOfComments = [commentArray count];
    // ex. localKeyFragment = "comment"
    if (numberOfComments == 1) {
        // suffix becomes "1_comment"
        localKeySuffix = [@"1_" stringByAppendingString:localKeyFragment];
    } else {
        // suffix becomes "n_comments"
        localKeySuffix = [NSString stringWithFormat:@"n_%@s", localKeyFragment];
    }
    
    // full key becomes "view.response.text.n_comments", for example
    NSString *fullKey = [@"view.response.text." stringByAppendingString:localKeySuffix];
    
    // localized title becomes "%lu Comments";
    NSString *localizedTitle =  DFLocalizedString(fullKey, nil);
    
    // detail string becomes "12 Comments", for example
    NSString *detailString = [NSString stringWithFormat:localizedTitle, numberOfComments];
    return detailString;
    /* prepends fontawesome icon
     // iconKey becomes icon.comments.comment
     NSString *iconKey = [@"icon.comments." stringByAppendingString:localKeyFragment];
     
     NSString *finalString = [detailString stringByPrependingIconForKey:iconKey];
     return finalString;
     */
}

- (DFObjectSection<id<DFCommentType>>*)objectSectionForCommentArray:(NSArray<id<DFCommentType>>*)commentArray withTitleKeyFragment:(NSString*)titleKeyFragment {
    NSString *commentSectionTitle = [self commentSectionTitleForKeyFragment:titleKeyFragment withCommentArray:commentArray];
    return [DFObjectSection objectSectionNamed:commentSectionTitle withObjects:commentArray];
}

// this method decides what to do with the various types of comments
- (void)organizeCommentSectionsFromContentObject:(id)contentObject {
    
    NSSortDescriptor *commentsSortDescriptor, *internalCommentsSortDescriptor, *researcherCommentsSortDescriptor;
    commentsSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"commentId" ascending:YES];
    internalCommentsSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"internalCommentId" ascending:YES];
    researcherCommentsSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"researcherCommentId" ascending:YES];
    
    
    NSMutableArray *tmpSections = [NSMutableArray array];
    NSMutableArray<NSArray<NSNumber*>*> *tmpHeights = [NSMutableArray array];
    
    self.comments = [[[contentObject comments] allObjects] sortedArrayUsingDescriptors:@[commentsSortDescriptor]];
    
    // everyone can see comments, no reason to check permissions
    DFObjectSection<id<DFCommentType>> *commentSection = [self objectSectionForCommentArray:self.comments withTitleKeyFragment:@"comment"];
    
    if (commentSection) {
        [tmpSections addObject:commentSection];
        [tmpHeights addObject:[self heightArrayForComments:self.comments]];
    }
    
    if ([LS.myUserPermissions canViewResearcherComments]) {
        self.researcherComments = [[[contentObject researcherComments] allObjects] sortedArrayUsingDescriptors:@[researcherCommentsSortDescriptor]];
        DFObjectSection<id<DFCommentType>> *researcherCommentSection = [self objectSectionForCommentArray:self.researcherComments withTitleKeyFragment:@"researcher_comment"];
        if (researcherCommentSection) {
            [tmpSections addObject:researcherCommentSection];
            [tmpHeights addObject:[self heightArrayForComments:self.researcherComments]];
        }
    }
    
    if ([LS.myUserPermissions canViewInternalComments]) {
        self.internalComments = [[[contentObject internalComments] allObjects] sortedArrayUsingDescriptors:@[internalCommentsSortDescriptor]];
        DFObjectSection<id<DFCommentType>> *internalCommentSection = [self objectSectionForCommentArray:self.internalComments withTitleKeyFragment:@"internal_comment"];
        if (internalCommentSection) {
            [tmpSections addObject:internalCommentSection];
            [tmpHeights addObject:[self heightArrayForComments:self.internalComments]];
        }
    }
    
    self.commentSections = [NSArray arrayWithArray:tmpSections];
    self.cellHeights = [NSArray arrayWithArray:tmpHeights];
}

- (void)organizeMediaFilesFromContentObject:(id)contentObject {
    self.responseBodyMediaFiles = [[contentObject files] allObjects];
    [self setGalleryFilesWithMediaFiles:self.responseBodyMediaFiles];
}


// gets the proper heights for cells based on an array of comments
- (NSArray<NSNumber*>*)heightArrayForComments:(NSArray<id<DFCommentType>>*)commentArray {
    
    NSMutableArray *heightArray = [NSMutableArray array];
    CGFloat width = self.view.frame.size.width;
    DFCommentCell *cell = [self.tableView dequeueReusableCellWithIdentifier:[DFCommentCell reuseIdentifier]];
    for (id<DFCommentType> comment in commentArray) {
        [self configureCommentCell:cell withComment:comment];
        [heightArray addObject:@([cell heightForWidth:width])];
    }
    return [NSArray arrayWithArray:heightArray];
}


- (void)calculateResponseBodyDimensions {
    CGFloat iconsPerRow = (CGFloat)kDFResponseBodyNumberOfMediaIconsPerRow;
    CGFloat padding = kDFResponseBodyPaddingBetweenMediaCells;
    
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    // (iconsPerRow + 1) * padding + iconsPerRow * iconSideLength = screenWidth
    CGFloat iconSideLength =  (screenWidth - padding) / iconsPerRow - padding;
    _responseBodyMediaIconSize = CGSizeMake(iconSideLength, iconSideLength);
    
    CGFloat numberOfRows = ceil((CGFloat)self.responseBodyMediaFiles.count / iconsPerRow);
    _mediaCollectionViewHeight = numberOfRows * iconSideLength + ((numberOfRows > 1) ? (numberOfRows - 1) * kDFResponseBodyPaddingBetweenMediaCells : 0);
    
    
    // Calculate response body actual height
    DFResponseBodyCell *responseBodyCell = [self.tableView dequeueReusableCellWithIdentifier:[DFResponseBodyCell reuseIdentifier]];
    [self configureResponseBodyCell:responseBodyCell];
    CGFloat height = [responseBodyCell heightWithoutCollectionViewForWidth:self.view.frame.size.width];
    height += _mediaCollectionViewHeight + padding;
    _responseBodyCellHeight = height;
    
}

#pragma mark - AddCommentController

- (void)addComment:(NSString*)commentText withThreadId:(NSNumber*)threadId {
    [self.addCommentController addCommentText:commentText withThreadId:threadId];
}

- (void)addCommentControllerDidRequestPopupMenu:(AddCommentController *)controller {
    
    CGRect popoverOriginRect = [self.addNoteButton convertRect:self.addNoteButton.bounds toView:self.view];
    [self.addCommentPopoverController presentPopoverFromRect:popoverOriginRect
                                                      inView:self.view
                                    permittedArrowDirections:WYPopoverArrowDirectionUp
                                                    animated:TRUE];
    self.addCommentPopoverController.popoverContentSize = [self.addCommentController tableViewSize];
}

- (void)addCommentControllerDidSendComment:(AddCommentController *)controller {
    [_addCommentTextView resignFirstResponder];
    if (self.addCommentPopoverController.popoverVisible) {
        [self.addCommentPopoverController dismissPopoverAnimated:true];
    }
    [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
}

-(void)addCommentController:(AddCommentController *)controller didGetServerResponseWithComment:(id<DFCommentType>)comment error:(NSError*)error {
    
    if (error) {
        [self.addCommentTextView becomeFirstResponder];
        [self.customAlert showAlertWithMessage:DFLocalizedString(@"view.response.error.comment_note_send_failure", nil) inView:self.view withTag:0];
    } else {
        /*
         NSInteger section = [sself numberOfSectionsInTableView:sself.tableView]-1;
         NSInteger row = [sself tableView:sself.tableView numberOfRowsInSection:section]-1;
         [sself.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:row inSection:section] atScrollPosition:UITableViewScrollPositionBottom animated:YES];*/
        [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
        
        self.addCommentTextView.text = @"";
        
        // scroll to new comment
        // Not sure why this works.
        
        //TODO: rewrite this
        /*CGFloat scrollY = (CGFloat)[[self.heightArray valueForKeyPath:@"@sum.self"] doubleValue];
         CGFloat tblHeight = self.tableView.bounds.size.height;
         CGRect rect = CGRectMake(0, scrollY-tblHeight, self.tableView.bounds.size.width, tblHeight);
         NSLog(@"%@", NSStringFromCGRect(rect));
         [self.tableView scrollRectToVisible:rect animated:YES];
         */
        
        //[sself.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:_cellsArray.count-1 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
        
        [self addCommentToDataModel:comment];
    }
}


- (void)addCommentController:(AddCommentController *)controller didRequestWriteNoteViewControllerDismissal:(DFWriteNoteViewController*)writeNoteViewController {
    CGRect viewFrame = writeNoteViewController.view.frame;
    viewFrame.origin.y = [UIScreen mainScreen].bounds.size.height;
    
    [UIView animateWithDuration:kDFAnimationDuration animations:^{
        writeNoteViewController.view.frame = viewFrame;
        self.keyboardAvoidingView.keyboardHeight.constant = 0.0;
        [self.keyboardAvoidingView layoutIfNeeded];
        [self displayElementsBasedOnPermissions];
        
    } completion:^(BOOL finished) {
        [writeNoteViewController.view removeFromSuperview];
        self.writeNoteViewController = nil;
        self.keyboardAvoidingView.avoidsKeyboard = true;
    }];
}

- (void)addCommentController:(AddCommentController *)controller didRequestWriteNoteControllerWithAction:(AddCommentAction*)action {
    
    if ([self.addCommentPopoverController isPopoverVisible]) {
        [self.addCommentPopoverController dismissPopoverAnimated:YES];
    }
    self.keyboardAvoidingView.avoidsKeyboard = false;
    self.messageTextView.hidden = true;
    
    self.writeNoteViewController = [[DFWriteNoteViewController alloc] initWithAction:action];
    self.writeNoteViewController.delegate = self.addCommentController;
    
    // shouldn't really present it this way but whatever
    [self.navigationController.view addSubview:self.writeNoteViewController.view];
    __block CGRect viewFrame = self.navigationController.view.frame;
    viewFrame.origin.y = [UIScreen mainScreen].bounds.size.height;
    self.writeNoteViewController.view.frame = viewFrame;
    [self.writeNoteViewController.view layoutIfNeeded];
    [UIView animateWithDuration:kDFAnimationDuration animations:^{
        viewFrame.origin.y = 0;
        self.writeNoteViewController.view.frame = viewFrame;
        self.commentTextViewHeightConstraint.constant = 0;
        [self.view layoutIfNeeded];
    }];
}

- (void)addCommentController:(AddCommentController *)controller writeNoteControllerDidRequestResponsePreview:(DFWriteNoteViewController *)writeNoteViewController {
    // slide down the writeNoteViewController
    if (!_isAnimating) {
        _isAnimating = true;
        self.viewState = kDFResponseViewStatePreview;
        writeNoteViewController.viewState = kDFWriteNoteViewStateMinimized;
        // animate to change states
        CGFloat ySplit = [UIScreen mainScreen].bounds.size.height * 2.0 / 3.0;
        [self setYSplit:ySplit withViewController:writeNoteViewController completion:^(BOOL success) {
            _isAnimating = false;
        }];
    }
    
}

- (void)addCommentController:(AddCommentController *)controller writeNoteControllerDidRequestFullScreen:(DFWriteNoteViewController *)writeNoteViewController {
    
    if (!_isAnimating) {
        _isAnimating = true;
        // slide up the writeNoteViewController
        self.viewState = kDFResponseViewStateNormal;
        writeNoteViewController.viewState = kDFWriteNoteViewStateFullScreen;
        
        // animate to change states
        [self setYSplit:0.0 withViewController:writeNoteViewController completion:^(BOOL success) {
            _isAnimating = false;
        }];
    }
}

#pragma mark - API Methods

-(void)fetchDailyDiaryWithDiaryID:(NSInteger)diaryID
{
    [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    NSNumber *threadId = 0;
    if (self.notification) {
        threadId = self.notification.referenceId;
    } else if (self.diary) {
        threadId = self.diary.threadId;
    }
    defwself
    [DFClient makeRequest:APIPathGetDailyDiary
                   method:kPOST
                urlParams:@{@"diaryId" : @(diaryID)}
               bodyParams:nil
                  success:^(NSDictionary *response, DailyDiary *dailyDiary) {
                      defsself
                      [MBProgressHUD hideHUDForView:sself.navigationController.view animated:YES];
                      LS.myUserInfo.currentProject.dailyDiary = dailyDiary;
                      for (Diary * d in dailyDiary.userDiaries) {
                          if ([d.threadId isEqualToNumber:threadId]) {
                              sself.diary = d;
                              break;
                          }
                      }
                      sself.dailyDiary = dailyDiary;
                      
                      if (sself.notification) {
                          if (sself.diary == nil) {
                              [sself.customAlert showAlertWithMessage:DFLocalizedString(@"view.response.alert.content_load_failure", nil) inView:sself.view withTag:0];
                              return;
                          }
                          [sself prepareAndLoadData];
                          NSInteger idx = [sself.comments indexOfObjectPassingTest:^BOOL(Comment *c, NSUInteger idx, BOOL *stop) {
                              if ([c.commentId isEqualToNumber:sself.notification.referenceId]) {
                                  *stop = true;
                                  return true;
                              }
                              return false;
                          }];
                          if (idx == NSNotFound) {
                              idx = 0;
                          }
                          //TODO: scroll to notification in question
                          //NSIndexPath *indexPath = [NSIndexPath indexPathForRow:_cellsArray.count - [sself.diary.comments allObjects].count + idx inSection:0];
                          //[sself.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
                      } else {
                          
                          [sself.tableView reloadData];
                      }
                  }
                  failure:^(NSError *error) {
                      defsself
                      [MBProgressHUD hideHUDForView:sself.navigationController.view animated:YES];
                  }];
}

/*
 -(void)fetchActivityWithNotification:(Notification*)notification {
 
 [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
 defwself
 [DFClient makeRequest:APIPathProjectGetActivities
 method:kPOST
 urlParams:@{@"projectId" : LS.myUserInfo.currentProjectId,
 @"activityId" : notification.referenceId}
 bodyParams:nil
 success:^(NSDictionary *response, DiaryTheme *diaryTheme) {
 defsself
 BOOL found = false;
 for (NSInteger i = 0, n = diaryTheme.responses.count; i < n; ++i) {
 Response *response = diaryTheme.responses[i];
 if ([response.threadId isEqualToNumber:notification.referenceId2]) {
 found = true;
 sself.response = response;
 [sself prepareAndLoadData];
 break;
 }
 }
 
 [MBProgressHUD hideHUDForView:sself.navigationController.view animated:YES];
 }
 failure:^(NSError *error) {
 defsself
 [MBProgressHUD hideHUDForView:sself.navigationController.view animated:YES];
 [sself.customAlert showAlertWithMessage:NSLocalizedString(@"The content you requested could not be found.", nil) inView:sself.view withTag:0];
 }];
 [self.refreshControl endRefreshing];
 }
 */

-(void)getResponsesWithActivityId:(NSInteger)activityId
{
    [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    
    defwself
    [DFClient makeRequest:APIPathActivityGetResponses
                   method:kPOST
                urlParams:@{@"activityId" : @(activityId)}
               bodyParams:nil
                  success:^(NSDictionary *response, id result) {
                      defsself
                      if ([result isKindOfClass:[Response class]]) {
                          sself.response = result;
                      } else if ([result isKindOfClass:[NSArray class]]) {
                          if (sself.notification) {
                              for (Response *obj in result) {
                                  if ([sself.notification.referenceId isEqualToNumber:obj.threadId]) {
                                      sself.response = obj;
                                      break;
                                  }
                              }
                          } else {
                              sself.response = result[0];
                          }
                      } else return;
                      // [sself organizeData];
                      //for (Comment *comment in [sself.response comments]) {
                      //    NSLog(@"%@", comment);
                      //}
                      if (sself.notification) {
                          [sself prepareAndLoadData];
                          NSInteger idx = [sself.comments indexOfObjectPassingTest:^BOOL(Comment *c, NSUInteger idx, BOOL *stop) {
                              if ([c.commentId isEqualToNumber:sself.notification.referenceId2]) {
                                  *stop = true;
                                  return true;
                              }
                              return false;
                          }];
                          
                          if (idx == NSNotFound) {
                              [sself.customAlert showAlertWithMessage:DFLocalizedString(@"view.response.alert.content_load_failure", nil) inView:sself.view withTag:0];
                              NSLog(@"Could not find a comment with id %@", sself.notification.referenceId2);
                          } else {
                              //TODO: Scroll to notification in question
                              //NSIndexPath *indexPath = [NSIndexPath indexPathForRow:_cellsArray.count - sself.response.comments.count + idx inSection:0];
                              // [sself.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
                          }
                      } else {
                          [sself.tableView reloadData];
                      }
                      [MBProgressHUD hideHUDForView:sself.navigationController.view animated:YES];
                  }
                  failure:^(NSError *error) {
                      defsself
                      
                      [MBProgressHUD hideHUDForView:sself.navigationController.view animated:YES];
                  }];
}


- (void)markCommentRead:(Comment*)comment {
    if ([comment respondsToSelector:@selector(isRead)]) {
        if (!comment.isRead.boolValue) {
            [DFClient makeRequest:APIPathActivityMarkCommentRead
                           method:kPOST
                        urlParams:@{@"commentId" : comment.commentId}
                       bodyParams:nil
                          success:nil
                          failure:^(NSError *error) {
                              comment.isRead = @NO;
                          }];
            comment.isRead = @YES;
        }
    }
}

#pragma mark - Table view data source
- (Comment*)commentForIndexPath:(NSIndexPath*)indexPath {
    return self.commentSections[indexPath.section-1].objects[indexPath.row];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections (1 for the response body + the number of comment sections)
    return 1 + self.commentSections.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{ // first section = response body
    if (indexPath.section == 0) {
        return _responseBodyCellHeight;
    } else {
        return [self.cellHeights[indexPath.section-1][indexPath.row] floatValue];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // first section = response body
    if (section == 0) {
        return 1;
    } else {
        // following sections = comment sections
        return [[self.commentSections[section-1] objects] count];
    }
}

-(void)configureResponseBodyCell:(DFResponseBodyCell*)cell
{
    NSString *responseText = nil;
    id contentObject = nil;
    if (_response != nil) {
        if (_response.textareaResponses.count>0) {
            TextareaResponse * textResponse = [_response.textareaResponses anyObject];
            responseText = textResponse.response;
        } else if (_response.imageGalleryResponses.count>0) {
            ImageGalleryResponse *imageGalleryResponse = [_response.imageGalleryResponses anyObject];
            responseText = imageGalleryResponse.response;
        } else {
            responseText = @"";
        }
        contentObject = _response;
    }
    else if (_diary != nil){
        responseText = _diary.response;
        contentObject = _diary;
    }
    
    [cell.responseBodyLabel setHTML:responseText];
    cell.dateLabel.text = [contentObject dateCreatedFormatted];
    cell.userNameLabel.text = [[contentObject userInfo] appUserName];
    
    [cell.avatarImageView setAvatarFromUserInfo:[contentObject userInfo]];
}

- (void)configureCommentCell:(DFCommentCell*)cell withComment:(id<DFCommentType>)comment {
    cell.dateLabel.text = comment.dateCreatedFormatted;
    cell.userNameLabel.text = comment.userInfo.appUserName;
    [cell.commentBodyLabel setHTML:comment.response];
    
    [cell.avatarImageView setAvatarFromUserInfo:comment.userInfo];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // first section has 1 row: response body
    if (indexPath.section == 0) {
        DFResponseBodyCell *cell = [tableView dequeueReusableCellWithIdentifier:[DFResponseBodyCell reuseIdentifier] forIndexPath:indexPath];
        [self configureResponseBodyCell:cell];
        return cell;
    } else {
        DFCommentCell *cell = [tableView dequeueReusableCellWithIdentifier:[DFCommentCell reuseIdentifier] forIndexPath:indexPath];
        [self configureCommentCell:cell withComment:[self commentForIndexPath:indexPath]];
        return cell;
    }
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    // no header for first section (the response body)
    if (section == 0) {
        return nil;
    } else {
        DFCommentSectionHeaderView *sectionHeaderView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:[DFCommentSectionHeaderView reuseIdentifier]];
        sectionHeaderView.titleLabel.text = self.commentSections[section-1].sectionName;
        return sectionHeaderView;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    // no header for first section (the response body)
    if (section == 0) {
        return 0;
    } else {
        return 40.0;
    }
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0 && indexPath.row == 0) {
        // assume this is the response body cell
        _responseBodyCell = (DFResponseBodyCell*)cell;
        _responseBodyCell.mediaCollectionView.delegate = self;
        _responseBodyCell.mediaCollectionView.dataSource = self;
        _responseBodyCell.mediaCollectionViewHeight = _mediaCollectionViewHeight;
        [_responseBodyCell.mediaCollectionView reloadData];
        [_responseBodyCell.mediaCollectionView addGestureRecognizer:self.mediaCollectionViewTapGestureRecognizer];
        
    } else if (indexPath.section > 0) {
        // assume this is a comment
        Comment *comment = [self commentForIndexPath:indexPath];
        [self markCommentRead:comment];
    }
}

- (void)tableView:(UITableView *)tableView didHighlightRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    [[tableView cellForRowAtIndexPath:indexPath] setHighlighted:false animated:NO];
}

#pragma mark - UICollectionViewDataSource (Response Body Media)
-(UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    File *mediaFile = self.responseBodyMediaFiles[indexPath.row];
    DFMediaCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[DFMediaCollectionViewCell reuseIdentifier] forIndexPath:indexPath];
    
    cell.isVideo = mediaFile.isVideo;
    [cell.imageView sd_setImageWithURL:mediaFile.previewImageURL placeholderImage:nil];
    return cell;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.responseBodyMediaFiles.count;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return !!self.responseBodyMediaFiles.count; // returns 0 or 1
}


#pragma mark - Feaux UICollectionViewDelegate method
- (void)didTapMediaCollectionView:(UITapGestureRecognizer*)tapGestureRecognizer {
    CGPoint touchPoint = [tapGestureRecognizer locationInView:_responseBodyCell.mediaCollectionView];
    NSIndexPath *indexPath = [_responseBodyCell.mediaCollectionView indexPathForItemAtPoint:touchPoint];
    self.galleryController = nil;
    self.galleryController.presentationIndex = indexPath.item;
    [self presentMHGalleryController:self.galleryController animated:YES completion:nil];
}


#pragma mark - UICollectionViewDelegateFlowLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return _responseBodyMediaIconSize;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return kDFResponseBodyPaddingBetweenMediaCells;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return kDFResponseBodyPaddingBetweenMediaCells;
}

//TODO: Refactor/rewrite
#pragma mark - ImagesCellDelegate
-(void)imageCell:(id)cell didClickOnButton:(id)button atIndex:(NSInteger)index atFile:(File *)file
{
    selectedIndex = index;
    [self performSegueWithIdentifier:@"carosulSegue" sender:self];
}


#pragma mark - HPGrowingTextViewDelegate


-(void)growingTextView:(HPGrowingTextView *)growingTextView willChangeHeight:(float)height {
    self.responseHeight.constant = height;
    [UIView animateWithDuration:self.addCommentTextView.animationDuration animations:^{
        [self.addCommentTextView layoutIfNeeded];
    }];
    
}


#pragma mark - CoreData

- (NSManagedObjectContext*)managedObjectContext {
    return [RKObjectManager sharedManager].managedObjectStore.mainQueueManagedObjectContext;
}

- (void)addCommentToDataModel:(id)comment {
    SEL addCommentSelector;
    
    if ([comment isKindOfClass:[Comment class]]) {
        addCommentSelector = @selector(addCommentsObject:);
    } else if ([comment isKindOfClass:[InternalComment class]]) {
        addCommentSelector = @selector(addInternalCommentsObject:);
    } else if ([comment isKindOfClass:[ResearcherComment class]]) {
        addCommentSelector = @selector(addResearcherCommentsObject:);
    } else {
        return;
    }
    
    [comment performSelector:@selector(setUserInfo:) withObject:LS.myUserInfo];
    if (self.response) {
        // this shouldn't cause a leak so long as the above code is OK
        [self.response performSelector:addCommentSelector withObject:comment];
    }
    if (self.diary) {
        // this shouldn't cause a leak either
        [self.diary performSelector:addCommentSelector withObject:comment];
    }
    [comment setDateCreatedFormatted:DFLocalizedString(@"app.dates.text.just_now", nil)];
    [self.managedObjectContext save:nil];
    [self prepareAndLoadData];
    
    [self scrollToComment:comment];
}

@end
