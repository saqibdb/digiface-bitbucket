//
//  AddCommentControllerDelegate.h
//  
//
//  Created by James on 3/13/16.
//
//

#import <Foundation/Foundation.h>

@class AddCommentController, AddCommentAction, DFWriteNoteViewController;

@protocol AddCommentControllerDelegate <NSObject>

- (void)addCommentControllerDidSendComment:(AddCommentController *)controller;

- (void)addCommentController:(AddCommentController *)controller didGetServerResponseWithComment:(id)comment error:(NSError*)error;

- (void)addCommentControllerDidRequestPopupMenu:(AddCommentController *)controller;

- (void)addCommentController:(AddCommentController *)controller didRequestWriteNoteControllerWithAction:(AddCommentAction*)action;

- (void)addCommentController:(AddCommentController *)controller didRequestWriteNoteViewControllerDismissal:(DFWriteNoteViewController*)writeNoteViewController;

- (void)addCommentController:(AddCommentController *)controller writeNoteControllerDidRequestResponsePreview:(DFWriteNoteViewController*)writeNoteViewController;

- (void)addCommentController:(AddCommentController *)controller writeNoteControllerDidRequestFullScreen:(DFWriteNoteViewController*)writeNoteViewController;

@property(nullable, nonatomic,readonly,strong) UINavigationController *navigationController;

@end
