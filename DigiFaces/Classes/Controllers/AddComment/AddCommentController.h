//
//  AddCommentController.h
//  DigiFaces
//
//  Created by James on 3/13/16.
//  Copyright © 2016 INET360. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AddCommentControllerDelegate.h"
#import "DFWriteNoteViewControllerDelegate.h"


@interface AddCommentController : UITableViewController<DFWriteNoteViewControllerDelegate>

@property (nonatomic, weak) id<AddCommentControllerDelegate> delegate;

- (void)addCommentText:(NSString *)commentText withThreadId:(NSNumber *)threadId;
- (void)addNoteWithThreadId:(NSNumber*)threadId;

- (CGSize)tableViewSize;

- (BOOL)shouldShowCommentInputTextView;
- (BOOL)shouldShowAddNoteButton;

@end
