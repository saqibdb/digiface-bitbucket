//
//  AddCommentController.m
//  DigiFaces
//
//  Created by James on 3/13/16.
//  Copyright © 2016 INET360. All rights reserved.
//

#import "AddCommentController.h"
#import "DFWriteNoteViewController.h"

#import "AddInternalCommentAction.h"
#import "AddResearcherCommentAction.h"

#import "NSString+DigiFaces.h"

#import "Comment.h"

#import "JPSemiModalAnimatedTransition.h"

static NSString *addCommentOptionCellReuseID = @"tableview.add_comment.option_cell";
static CGFloat commentCellHeight = 40.0;

@interface AddCommentController () <UITableViewDataSource, UITableViewDelegate> {
    
    NSNumber *_threadId;
    NSString *_commentText;
}

@property (nonatomic, strong) NSArray<AddCommentAction*> *optionActions;

@property (nonatomic, strong) AddCommentAction *addCommentAction;


@end

@implementation AddCommentController

#pragma mark - Initialization

- (instancetype)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        [self initTitlesAndActions];
    }
    return self;
}


#pragma mark - Custom Methods

- (BOOL)shouldShowCommentInputTextView {
    return [LS.myUserPermissions canAddComments];
}

- (BOOL)shouldShowAddNoteButton {
    return [LS.myUserPermissions canAddInternalComments]
    || [LS.myUserPermissions canAddResearcherComments];
}


- (CGSize)tableViewSize {
    CGFloat height = (CGFloat)self.optionActions.count * commentCellHeight;
    CGFloat width = 200.0;
    return CGSizeMake(width, height);
}

- (AddCommentAction*)addCommentAction {
    if (!_addCommentAction) {
        _addCommentAction = [[AddCommentAction alloc] init];
    }
    return _addCommentAction;
}

#pragma mark - Add Comment

- (void)addCommentText:(NSString *)commentText withThreadId:(NSNumber *)threadId {
    _commentText = commentText;
    _threadId = threadId;
    
    // this is for adding COMMENTs not NOTEs
    [self addCommentWithAction:self.addCommentAction];
    
}

- (void)addNoteWithThreadId:(NSNumber *)threadId {
    _threadId = threadId;
    
    if ([self.optionActions count] == 1) {
        // only one type of comment available.  send using the only action we have
        [self showWriteNoteControllerWithAction:self.optionActions.firstObject];
        
    } else if ([self.optionActions count] > 1) {
        // request popup from delegate (multiple options available)
        if ([self.delegate respondsToSelector:@selector(addCommentControllerDidRequestPopupMenu:)]) {
            [self.delegate addCommentControllerDidRequestPopupMenu:self];
        }
    }
}

- (void)addCommentWithAction:(AddCommentAction*)action {
    if (action) {
        if ([self.delegate respondsToSelector:@selector(addCommentControllerDidSendComment:)]) {
            [self.delegate addCommentControllerDidSendComment:self];
        }
        
        defwself
        [action executeWithCommentText:_commentText threadId:_threadId completion:^(id comment, NSError *error) {
            defsself
            [sself didGetServerResponseWithComment:comment error:error];
        }];
    }
    
}

- (void)didGetServerResponseWithComment:(id<DFCommentType>)comment error:(NSError*)error {
    if ([self.delegate respondsToSelector:@selector(addCommentController:didGetServerResponseWithComment:error:)]) {
        [self.delegate addCommentController:self didGetServerResponseWithComment:comment error:error];
    }
}

#pragma mark - DFWriteNoteViewController

- (void)showWriteNoteControllerWithAction:(AddCommentAction*)action {
    
    if ([self.delegate respondsToSelector:@selector(addCommentController:didRequestWriteNoteControllerWithAction:)]) {
        [self.delegate addCommentController:self didRequestWriteNoteControllerWithAction:action];
    }
}

#pragma mark - DFWriteNoteViewControllerDelegate

- (void)writeNoteViewController:(DFWriteNoteViewController *)writeNoteViewController didGetServerResponseWithComment:(id<DFCommentType>)comment error:(NSError*)error {
    [self didGetServerResponseWithComment:comment error:error];
}

- (void)writeNoteViewControllerDidRequestDismissal:(DFWriteNoteViewController *)writeNoteViewController {
    if ([self.delegate respondsToSelector:@selector(addCommentController:didRequestWriteNoteViewControllerDismissal:)]) {
        [self.delegate addCommentController:self didRequestWriteNoteViewControllerDismissal:writeNoteViewController];
    }
}

- (void)writeNoteViewControllerDidRequestResponsePreview:(DFWriteNoteViewController *)writeNoteViewController {
    if ([self.delegate respondsToSelector:@selector(addCommentController:writeNoteControllerDidRequestResponsePreview:)]) {
        [self.delegate addCommentController:self writeNoteControllerDidRequestResponsePreview:writeNoteViewController];
    }
}

- (void)writeNoteViewControllerDidRequestFullScreen:(DFWriteNoteViewController*)writeNoteViewController {
    if ([self.delegate respondsToSelector:@selector(addCommentController:writeNoteControllerDidRequestFullScreen:)]) {
        [self.delegate addCommentController:self writeNoteControllerDidRequestFullScreen:writeNoteViewController];
    }
}

- (NSNumber*)threadId {
    return _threadId;
}

#pragma mark - UITableView setup


- (void)initTitlesAndActions {
    NSMutableArray *mutableActions = [NSMutableArray array];
    
    
    if ([LS.myUserPermissions canAddResearcherComments]) {
        [mutableActions addObject:[[AddResearcherCommentAction alloc] init]];
    }
    
    if ([LS.myUserPermissions canAddInternalComments]) {
        [mutableActions addObject:[[AddInternalCommentAction alloc] init]];
    }
    
    /* comments are not handled by this popup anymore
     if ([LS.myUserPermissions canAddComments]) {
     [mutableTitles addObject:[@"verb.comment.add_comment" localizedAndPrefixedWithIconForKey:@"icon.comments.comment"]];
     [mutableActions addObject:[[AddCommentAction alloc] init]];
     }*/
    
    self.optionActions = [NSArray arrayWithArray:mutableActions];
    
}


#pragma mark - UITableViewDataSource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 40.0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.optionActions.count;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = nil;
    
    if (tableView == self.tableView) {
        if (!(cell = [tableView dequeueReusableCellWithIdentifier:addCommentOptionCellReuseID])) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:addCommentOptionCellReuseID];
            cell.textLabel.font = [UIFont fontWithName:@"FontAwesome" size:14.0];
            cell.separatorInset = UIEdgeInsetsZero;
            cell.layoutMargins = UIEdgeInsetsZero;
            cell.preservesSuperviewLayoutMargins = NO;
        }
        cell.textLabel.text = self.optionActions[indexPath.row].localizedActionTitle;
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == self.tableView) {
        [tableView deselectRowAtIndexPath:indexPath animated:NO];
        AddCommentAction *action = self.optionActions[indexPath.row];
        [self showWriteNoteControllerWithAction:action];
    }
}

@end
