//
//  AddCommentAction.h
//  DigiFaces
//
//  Created by James on 3/14/16.
//  Copyright © 2016 INET360. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DFCommentType.h"

typedef enum : NSUInteger {
    DFCommentTypeComment,
    DFCommentTypeInternal,
    DFCommentTypeResearcher,
} DFCommentType;

@interface AddCommentAction : NSObject

- (void)executeWithCommentText:(NSString*)commentText threadId:(NSNumber*)threadId completion:(void (^)(id<DFCommentType> comment, NSError *error))completionBlock;

// API interaction
- (NSString*)apiMethodPath;
- (NSString*)paramNameForCommentId;
- (NSDictionary*)paramsWithCommentText:(NSString*)commentText threadId:(NSNumber*)threadId;

// Localization
- (NSString*)localizedActionTitle;
- (NSString*)objectNameKey;
- (NSString*)actionNameKey;
- (NSString*)actionIconKey;
- (NSString*)typeKey;

@end
