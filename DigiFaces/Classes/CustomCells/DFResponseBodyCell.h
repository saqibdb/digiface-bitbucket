//
//  ResponseBodyCell.h
//  DigiFaces
//
//  Created by James on 3/18/16.
//  Copyright © 2016 INET360. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DFResponseBodyCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *responseBodyLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *mediaCollectionView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mediaCollectionViewHeightConstraint;

@property (nonatomic) CGFloat mediaCollectionViewHeight;

+ (NSString*)reuseIdentifier;
- (CGFloat)heightWithoutCollectionViewForWidth:(CGFloat)width;

@end
