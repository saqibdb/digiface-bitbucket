//
//  DFMediaCollectionViewCell.m
//  DigiFaces
//
//  Created by James on 3/19/16.
//  Copyright © 2016 INET360. All rights reserved.
//

#import "DFMediaCollectionViewCell.h"

@implementation DFMediaCollectionViewCell


+ (NSString*)reuseIdentifier {
    static NSString * const reuseIdentifier = @"DFMediaCollectionViewCell";
    return reuseIdentifier;
}

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
}

- (void)setIsVideo:(BOOL)isVideo {
    self.videoIndicatorImageView.hidden = !isVideo;
}

- (nullable UIView *)hitTest:(CGPoint)point withEvent:(nullable UIEvent *)event {
    return self;
}

@end
