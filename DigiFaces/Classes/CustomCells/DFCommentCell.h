//
//  CommentCell.h
//  DigiFaces
//
//  Created by confiz on 06/07/2015.
//  Copyright (c) 2015 Usasha studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DFCommentCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *commentBodyLabel;

+ (NSString*)reuseIdentifier;
- (CGFloat)heightForWidth:(CGFloat)width;
- (void)flash;

@end
