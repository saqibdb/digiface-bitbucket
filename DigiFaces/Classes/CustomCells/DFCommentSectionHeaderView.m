//
//  DFCommentSectionHeaderView.m
//  DigiFaces
//
//  Created by James on 3/19/16.
//  Copyright © 2016 INET360. All rights reserved.
//

#import "DFCommentSectionHeaderView.h"

@implementation DFCommentSectionHeaderView


+ (NSString*)reuseIdentifier {
    static NSString * const reuseIdentifier = @"DFCommentSectionHeaderView";
    return reuseIdentifier;
}

@end
