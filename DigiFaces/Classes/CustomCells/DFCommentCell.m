//
//  CommentCell.m
//  DigiFaces
//
//  Created by confiz on 06/07/2015.
//  Copyright (c) 2015 Usasha studio. All rights reserved.
//

#import "DFCommentCell.h"

@implementation DFCommentCell

+ (NSString*)reuseIdentifier {
    static NSString * const reuseIdentifier = @"DFCommentBodyTableViewCell";
    return reuseIdentifier;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initUI];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self initUI];
}

- (void)initUI {
    self.separatorInset = UIEdgeInsetsZero;
    self.layoutMargins = UIEdgeInsetsZero;
    self.preservesSuperviewLayoutMargins = NO;
    
    self.avatarImageView.layer.borderColor = [UIColor colorWithWhite:0.90 alpha:1.0].CGColor;
    self.avatarImageView.layer.borderWidth = 1.0;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (CGFloat)heightForWidth:(CGFloat)width {
    CGFloat padding = 8.0; // default for auto-layout
    CGFloat bodyLabelOffset = self.commentBodyLabel.frame.origin.y;
    CGFloat bodyLabelHeight = [self.commentBodyLabel sizeThatFits:CGSizeMake(width - self.avatarImageView.frame.size.width - padding*3.0, CGFLOAT_MAX)].height;
    return  bodyLabelOffset + bodyLabelHeight + padding * 2.0;
}

- (void)flash {
    UIColor *initialColor = [UIColor whiteColor];
    UIColor *flashColor = [UIColor colorWithRed:65.0/255.0 green:152.0/255.0 blue:254.0/255.0 alpha:1.0];
    [UIView animateWithDuration:kDFAnimationDuration animations:^{
        self.backgroundColor = flashColor;
        [UIView animateWithDuration:kDFAnimationDurationLong animations:^{
            self.backgroundColor = initialColor;
        }];
    }];
}

@end
