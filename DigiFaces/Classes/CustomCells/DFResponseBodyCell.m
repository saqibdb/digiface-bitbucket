//
//  ResponseBodyCell.m
//  DigiFaces
//
//  Created by James on 3/18/16.
//  Copyright © 2016 INET360. All rights reserved.
//

#import "DFResponseBodyCell.h"

#import "DFMediaCollectionViewCell.h"

@implementation DFResponseBodyCell

+ (NSString*)reuseIdentifier {
    static NSString * const reuseIdentifier = @"DFResponseBodyTableViewCell";
    return reuseIdentifier;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initUI];
    }
    return self;
}

- (void)awakeFromNib {
    [self initUI];
}

- (void)initUI {
    // Initialization code
    self.separatorInset = UIEdgeInsetsZero;
    self.layoutMargins = UIEdgeInsetsZero;
    self.preservesSuperviewLayoutMargins = NO;
    
    self.avatarImageView.layer.borderColor = [UIColor colorWithWhite:0.81 alpha:1.0].CGColor;
    self.avatarImageView.layer.borderWidth = 1.5;
    
    [self.mediaCollectionView registerNib:[UINib nibWithNibName:@"DFMediaCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:[DFMediaCollectionViewCell reuseIdentifier]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (CGFloat)heightWithoutCollectionViewForWidth:(CGFloat)width {
    CGFloat padding = 8.0; // default spacing/padding for interface builder
    CGFloat bodyLabelOffset = self.responseBodyLabel.frame.origin.y;
    CGFloat bodyLabelHeight = [self.responseBodyLabel sizeThatFits:CGSizeMake(width-padding*2.0, CGFLOAT_MAX)].height;
    return padding + bodyLabelOffset + bodyLabelHeight + padding;
}

- (CGFloat)mediaCollectionViewHeight {
    return self.mediaCollectionViewHeightConstraint.constant;
}

- (void)setMediaCollectionViewHeight:(CGFloat)mediaCollectionViewHeight {
    self.mediaCollectionViewHeightConstraint.constant = mediaCollectionViewHeight;
}

@end
