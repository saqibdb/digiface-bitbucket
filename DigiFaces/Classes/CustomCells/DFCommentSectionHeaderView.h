//
//  DFCommentSectionHeaderView.h
//  DigiFaces
//
//  Created by James on 3/19/16.
//  Copyright © 2016 INET360. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DFCommentSectionHeaderView : UITableViewHeaderFooterView

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;

+ (NSString*)reuseIdentifier;

@end
