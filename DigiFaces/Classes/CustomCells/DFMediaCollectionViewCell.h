//
//  DFMediaCollectionViewCell.h
//  DigiFaces
//
//  Created by James on 3/19/16.
//  Copyright © 2016 INET360. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DFMediaCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIImageView *videoIndicatorImageView;

@property (nonatomic) BOOL isVideo;

+ (NSString*)reuseIdentifier;

@end
