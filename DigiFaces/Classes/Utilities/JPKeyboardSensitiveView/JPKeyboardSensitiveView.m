//
//  JPKeyboardSensitiveView.m
//  Jarvis
//
//  Created by James on 2/16/15.
//  Copyright (c) 2015 James Perlman. All rights reserved.
//

#import "JPKeyboardSensitiveView.h"

@implementation JPKeyboardSensitiveView

#pragma mark - Keyboard-sensitive layout

- (void)awakeFromNib {
    self.avoidsKeyboard = true;
}

- (void)setAvoidsKeyboard:(BOOL)avoidsKeyboard {
    if (_avoidsKeyboard != avoidsKeyboard) {
        _avoidsKeyboard = avoidsKeyboard;
        if (avoidsKeyboard) {
            [self observeKeyboard];
        } else {
            [self stopObservingKeyboard];
        }
    }
}

- (void)observeKeyboard {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillChangeFrameNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)stopObservingKeyboard {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillChangeFrameNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWillShow:(NSNotification *)notification {
    NSDictionary *info = [notification userInfo];
    NSValue *kbFrame = info[UIKeyboardFrameEndUserInfoKey];
    NSTimeInterval animationDuration = [info[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    CGRect keyboardFrame = [kbFrame CGRectValue];
    
    CGFloat height = keyboardFrame.size.height;
    
    self.keyboardHeight.constant = height;
    
    [UIView animateWithDuration:animationDuration animations:^{
        [self.superview layoutIfNeeded];
    }];
}

- (void)keyboardWillHide:(NSNotification *)notification {
    NSDictionary *info = [notification userInfo];
    NSTimeInterval animationDuration = [info[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    self.keyboardHeight.constant = 0;
    [UIView animateWithDuration:animationDuration animations:^{
        [self.superview layoutIfNeeded];
    }];
}

- (void) dealloc {
    if (self.avoidsKeyboard) {
        [self stopObservingKeyboard];
    }
}
@end
