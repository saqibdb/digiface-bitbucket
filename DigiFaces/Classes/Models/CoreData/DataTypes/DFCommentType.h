//
//  DFCommentType.h
//  DigiFaces
//
//  Created by James on 3/18/16.
//  Copyright © 2016 INET360. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol DFCommentType <NSObject>

@property (nonatomic, retain) NSString * dateCreated;
@property (nonatomic, retain) NSString * dateCreatedFormatted;
@property (nonatomic, retain) NSNumber * isActive;
@property (nonatomic, retain) NSNumber * isRead;
@property (nonatomic, retain) NSString * response;
@property (nonatomic, retain) NSNumber * threadId;
@property (nonatomic, retain) NSString * userId;
@property (nonatomic, retain) UserInfo *userInfo;

@end
