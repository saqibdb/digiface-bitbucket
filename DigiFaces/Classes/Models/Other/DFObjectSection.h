//
//  DFObjectSection.h
//  DigiFaces
//
//  Created by James on 3/18/16.
//  Copyright © 2016 INET360. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DFObjectSection<__covariant ObjectType> : NSObject

@property (nonatomic, strong) NSString *sectionName;
@property (nonatomic, strong) NSArray<ObjectType> *objects;

- (ObjectType)objectAtIndexPath:(NSIndexPath*)indexPath;

- (instancetype)initWithSectionName:(NSString*)sectionName objects:(NSArray<ObjectType>*)objects;
+ (instancetype)objectSectionNamed:(NSString*)sectionName withObjects:(NSArray<ObjectType>*)objects;

@end

