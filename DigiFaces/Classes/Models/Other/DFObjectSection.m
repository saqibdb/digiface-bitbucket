//
//  DFObjectSection.m
//  DigiFaces
//
//  Created by James on 3/18/16.
//  Copyright © 2016 INET360. All rights reserved.
//

#import "DFObjectSection.h"


@implementation DFObjectSection

- (id)objectAtIndexPath:(NSIndexPath *)indexPath {
    id object;
    @try {
        object = self.objects[indexPath.section][indexPath.row];
    }
    @catch (NSException *exception) {
        object = nil;
    }
    return object;
}

- (instancetype)initWithSectionName:(NSString*)sectionName objects:(NSArray *)objects {
    self = [super init];
    if (self) {
        self.sectionName = sectionName ?: @"";
        
        if (!objects || ![objects count]) {
            self = nil;
        } else {
            self.objects = [objects copy];
        }
    }
    return self;
}

+ (instancetype)objectSectionNamed:(NSString *)sectionName withObjects:(NSArray *)objects {
    return [[self alloc] initWithSectionName:sectionName objects:objects];
}
@end
