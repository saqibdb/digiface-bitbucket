//
//  DFCircularImageView.m
//  DigiFaces
//
//  Created by James on 3/19/16.
//  Copyright © 2016 INET360. All rights reserved.
//

#import "DFCircularImageView.h"

@implementation DFCircularImageView

- (void)layoutMarginsDidChange {
    [super layoutMarginsDidChange];
    [self makeCircular];
}
- (void)layoutSubviews {
    [super layoutSubviews];
    [self makeCircular];
}

- (void)makeCircular {
    self.layer.cornerRadius = MAX(self.frame.size.width, self.frame.size.height)/2.0f;
    self.clipsToBounds = true;
}

@end
